package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.json.*;
import noobbot.json.carposition.CarPositionMessage;
import noobbot.json.init.Car;
import noobbot.json.init.InitMessage;
import noobbot.json.init.Track;
import noobbot.models.*;

import java.util.List;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
    	/*
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
    	*/
    	String botName = "Kurko";
    	String botKey = "nvp3B++x0zRIfQ";
        //String host = "webber.helloworldopen.com";
    	String host = "testserver.helloworldopen.com";
        int port = 8091;
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    // TODO: Move somewhere better place
    //private Track mTrack;
    private List<Car> mCars;

    private TrackModel mTrack;
    private CarModel mMyCar;
    private BotController mController;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        // We could create a typeadapter for parsin the message, but to save time it's done 'quick and dirty' :-)
        while((line = reader.readLine()) != null) {
            // create the message object from json
            // MsgWrapper.data contains the whole data object
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            String msgType = msgFromServer.msgType;
            
            if (msgType.equals("carPositions")) {
                // Update the car
            	CarPositionMessage carPosition = gson.fromJson(line, CarPositionMessage.class);
            	mController.update(carPosition);
            } else if (msgType.equals("yourCar")) {
                System.out.println(msgFromServer.data.toString());
            } else if (msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgType.equals("gameInit")) {
            	System.out.println(msgFromServer.data.toString());
                // Data is inside the object now..
                InitMessage m = gson.fromJson(line, InitMessage.class);
                mTrack = new TrackModel(m.data.race.track);
                
                // TODO: Find the proper car from "YourCar" message
                // Init the car and controller
                mMyCar = new CarModel(m.data.race.cars.get(0));
                mController = new BotController(mMyCar, mTrack, this);
                
           } else if (msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println(msgFromServer.data.toString());
            } else if (msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgType.equals("crash")) {
            	System.err.println("CRASH");
            } else {
                System.out.println("Another msg: " + msgType);
                send(new Ping());
            }
        }
    }

    public void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

// Received message
class MsgWrapper {
    public final String msgType;
    public final Object data;

    // TODO: Generics?

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}


// Commands
class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
