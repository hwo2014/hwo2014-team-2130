package noobbot;

import java.util.List;

import noobbot.json.carposition.CarPositionData;
import noobbot.json.carposition.CarPositionMessage;
import noobbot.json.init.Car;
import noobbot.json.init.CarPosition;
import noobbot.json.init.Track;
import noobbot.models.CarModel;
import noobbot.models.TrackModel;
import noobbot.models.TrackPieceModel;

/**
 * My controller
 *
 */
class BotController {
	private CarModel mMyCar;
	private TrackModel mTrack;
	private Main mMain;
	
	private boolean mEmergencyPowerOff;
	
	private List<CarModel> mCompetitors;
	
    public BotController(CarModel car, TrackModel track, Main main) {
    	mMyCar = car;
    	mTrack = track;
    	mMain = main;
    }

    public void update(CarPositionMessage carPosition) {
    	float speed = calculateCarSpeed(carPosition.data.get(0));
    	
    	mMyCar.update(carPosition.data.get(0));
    	mMyCar.setSpeed(speed);

    	// >58 == crash
    	int myPieceIndex = mMyCar.getPieceIndex();
    	int nextPieceIndex = 0;
    	
    	if(myPieceIndex == mTrack.pieceCount() -1) {
    		// We are in last piece
    		nextPieceIndex = 0;
    	} else {
    		nextPieceIndex = myPieceIndex + 1;
    	}

    	TrackPieceModel currentPiece = mTrack.getPiece(myPieceIndex);
    	TrackPieceModel nextPiece = mTrack.getPiece(nextPieceIndex);

    	System.out.println("speed            = " + mMyCar.getSpeed());
    	System.out.println("distance to turn = " + distanceToNextTurn());
    	/*
    	System.out.println("car - angle: " + mMyCar.getAngle() + ", position: " 
    			+ carPosition.data.get(0).piecePosition.pieceIndex + "/" 
    			+ carPosition.data.get(0).piecePosition.inPieceDistance + ", speed: " + speed);
    	
    	System.out.println("Next piece (" + nextPieceIndex + "): "  + nextPiece.getAngle() + "/" + nextPiece.getRadius() );
		*/
    	
    	// entering turn, decrease speed
    	float throttle = 0;
    	
    	
    	// speed, distance..
    	// speed = 0....10
    	// distance = 0...100
    	float whenToSlow = speed * 10 * 1.5f;
    	
    	float slowSpeed = (speed - 5) / 10;
    	if(slowSpeed < 0) 
    		slowSpeed = 0.2f;
    	
    	// Depending on the speed we need to slow down!
    	if(distanceToNextTurn() < whenToSlow) {
    		mMain.send(new Throttle(0.5));
    	}
    	/*
    	// vastaheitto
    	else if((mMyCar.getAngle() > 0 && currentPiece.getAngle() < 0) ||
    			(mMyCar.getAngle() < 0 && currentPiece.getAngle() > 0)) {
    		mMain.send(new Throttle(0.1));
    	}
    	*/
    	/*
    	else if(mMyCar.getAngle() > 5 || mMyCar.getAngle() < -5) {
    		mMain.send(new Throttle(0)); // Critical angle, slow down as soon as possible!
    	}
    	*/
    	else if(currentPiece.getAngle() > 0 && nextPiece.getAngle() == 0) {
    		mMain.send(new Throttle(0.7));
    	}
    	else if(currentPiece.getAngle() > 0) {
    		mMain.send(new Throttle(0.4));
    	}
    	else {
    		mMain.send(new Throttle(1.0));
    	}
    }
       
    private float distanceToNextTurn() {
    	int currentIndex = mMyCar.getPieceIndex();
    	int turnPiece = mTrack.nextTurn(currentIndex);
    	
    	return mTrack.distanceToPiece(mMyCar.getPieceIndex(), mMyCar.getPieceDistance(), 
    			turnPiece, 0);
    	
    }
    
    private float calculateCarSpeed(CarPositionData carPosition) {
    	// where we were
    	int oldPieceIndex = mMyCar.getPieceIndex(); 
    	float oldPieceDistance = mMyCar.getPieceDistance();
    	
    	// where we are
    	int currentPieceIndex = carPosition.piecePosition.pieceIndex;
    	float currentPieceDistance = carPosition.piecePosition.inPieceDistance;
    	
    	float distance = 0;
    	
    	// get the length of driven road
    	if(currentPieceIndex != oldPieceIndex) {
    		// Piece changed
    		TrackPieceModel oldPiece = mTrack.getPiece(oldPieceIndex);
    		
    		int startLane = carPosition.piecePosition.lane.startLaneIndex;
    		float oldPieceLength = oldPiece.getLength(startLane);
   		
    		distance = oldPieceLength - oldPieceDistance + currentPieceDistance;
    	} else {
    		// piece still the same
    		distance = currentPieceDistance - oldPieceDistance;
    	}
    	
    	return distance;
    }
}
