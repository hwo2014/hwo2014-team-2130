package noobbot.models;

import noobbot.json.*;
import noobbot.json.carposition.CarPositionData;
import noobbot.json.carposition.CarPositionMessage;
import noobbot.json.init.Track;
import noobbot.json.init.TrackPiece;

public class CarModel {
    private String mName;
    private String mColor;
    private float mAngle;
    
    private float mSpeed; // TODO: calculate speed from prev and current pos delta 
    
    private int mCurrentPiece;
    private float mCurrentPieceDistance;
    
    private boolean mMyCar;

    public CarModel(noobbot.json.init.Car car) {
        // init the car from the init resp model
    	mName = car.id.id;
    	mColor = car.id.color;

        mAngle = 0;
    }

   public void update(CarPositionData position) {
	   mAngle = position.angle;
	   mCurrentPiece = position.piecePosition.pieceIndex;
	   mCurrentPieceDistance = position.piecePosition.inPieceDistance;
   }
 
   public void setSpeed(float speed) {
	   mSpeed = speed;
   }
   
   public float getSpeed() {
	   return mSpeed;
   }
   
   public float getAngle() {
	   return mAngle;
   }
   
   public int getPieceIndex() {
	   return mCurrentPiece;
   }
   
   public float getPieceDistance() {
	   return mCurrentPieceDistance;
   }
}
