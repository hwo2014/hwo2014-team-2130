package noobbot.models;

import java.util.ArrayList;
import java.util.List;

import noobbot.json.init.Track;

public class TrackModel {
    public List<TrackPieceModel> mPieces;
    public List<LaneModel> mLanes;
    
	public TrackModel(Track track) {
		mPieces = new ArrayList<TrackPieceModel>();
		mLanes = new ArrayList<LaneModel>();
		
		for(int i=0; i < track.pieces.size(); i++) {
			TrackPieceModel piece = new TrackPieceModel(track.pieces.get(i), track);
			mPieces.add(piece);
		}
		
		for(int i=0; i < track.lanes.size(); i++) {
			LaneModel lane = new LaneModel(track.lanes.get(i));
			mLanes.add(lane);
		}
	}
	
	public TrackPieceModel getPiece(int index) {
		return mPieces.get(index);
	}
	
	public int pieceCount() {
		return mPieces.size();
	}
	
	public int nextTurn(int startIndex) {
		for(int i=startIndex; i<mPieces.size(); i++) {
			if(mPieces.get(i).getRadius() > 0) {
				return i;
			}
		}
		
		return -1;
	}
	
	public float distanceToPiece(int startIndex, float startPieceDistance, int targetIndex, int laneIndex) {
		float totalLength = 0;
		
		if(targetIndex < startIndex) {
			for(int i=startIndex; i < mPieces.size(); i++) {
				totalLength += mPieces.get(i).getLength(laneIndex);
			}
			
			for(int i=0; i < targetIndex; i++) {
				totalLength = mPieces.get(i).getLength(laneIndex);
			}
		} else if(startIndex == targetIndex) {
			return 0;
		} else {
			for(int i=startIndex; i < targetIndex; i++) {
				totalLength += mPieces.get(i).getLength(laneIndex);
			}
		}
		
		totalLength -= startPieceDistance;
		
		return totalLength;
	}
}
