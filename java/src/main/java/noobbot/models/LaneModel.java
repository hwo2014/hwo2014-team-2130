package noobbot.models;

import noobbot.json.init.Lane;

public class LaneModel {
	private int mLaneId;
	private float mDistanceFromCenter;
	
	public LaneModel(Lane lane) {
		mLaneId = lane.index;
		mDistanceFromCenter = lane.distanceFromCenter;
	}
}
