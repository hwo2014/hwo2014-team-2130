package noobbot.models;

import java.util.HashMap;
import java.util.List;

import noobbot.json.init.Lane;
import noobbot.json.init.Track;
import noobbot.json.init.TrackPiece;

public class TrackPieceModel {
	private float mAngle; // only available in arc
	private float mRadius; // only available in arc
	private HashMap<Integer, Float> mLength;
	private boolean mLaneSwitch = false; // only available in straights
	
	public TrackPieceModel(TrackPiece p, Track t) {
		mAngle = p.angle;
		mRadius = p.radius;
		mLength = new HashMap<Integer, Float>();
		
		// The track may have 1 to 4 lanes. 
		// The lanes are described in the protocol as an array of objects that indicate the lane's distance
		// from the track center line. A positive value tells that the lanes is to the right from the center 
		// line while a negative value indicates a lane to the left from the center.
		int laneCount = t.lanes.size();
		for(int i = 0; i < laneCount; i++) {
			// Calculate length for all lanes
			Lane lane = t.lanes.get(i);

			if(p.length != 0) {
				mLength.put(lane.index, p.length);
			} else {
				mLength.put(lane.index, arcLength(p.angle, p.radius, lane));
			}
		}

		mLaneSwitch = p.laneSwitch;
	}
	
	public float getAngle() {
		return mAngle;
	}
	
	public float getRadius() {
		return mRadius;
	}
	
	public float getLength(int laneIndex) {
		return mLength.get(laneIndex);
	}

	/**
	 * Calculates length of the piece for each lane
	 * @param angle
	 * @param radius
	 * @param lane
	 * @return
	 */
	private float arcLength(float angle, float radius, Lane lane) {
		double value = 0;
		if(angle > 0) {
			value = Math.abs((angle * Math.PI * (radius - lane.distanceFromCenter)) / 180);
		} else {
			value = Math.abs((angle * Math.PI * (radius + lane.distanceFromCenter)) / 180);
		}
		
		return (float) value;
	}
}
