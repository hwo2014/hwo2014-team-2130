package noobbot.json.init;

import java.util.List;

public class Race {
    public Track track;
    public List<Car> cars;
    public RaceSession raceSession;
}
