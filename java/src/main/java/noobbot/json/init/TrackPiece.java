package noobbot.json.init;

import com.google.gson.annotations.SerializedName;

public class TrackPiece {
    public float length;
    @SerializedName("switch")
    public boolean laneSwitch;
    public float radius;
    public float angle;
}
