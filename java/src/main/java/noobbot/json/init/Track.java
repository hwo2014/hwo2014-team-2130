package noobbot.json.init;

import java.util.List;

public class Track {
    public String id;
    public String name;
    public List<TrackPiece> pieces;
    public List<Lane> lanes;
    public StartingPoint startingPoint;
}
