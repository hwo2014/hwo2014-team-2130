package noobbot.json.init;

import com.google.gson.annotations.SerializedName;

public class CarPosition {
    public CarId id;
    @SerializedName("lane")
    public CarLane carLane;
    public float angle;
}
