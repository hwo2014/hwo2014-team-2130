package noobbot.json.carposition;

import java.util.List;

public class CarPositionMessage {
    public List<CarPositionData> data;
    public String msgType;
}
