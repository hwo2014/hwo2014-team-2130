package noobbot.json.carposition;

import noobbot.json.init.CarId;

public class CarPositionData {
	public CarId id;
	public float angle;
	public PiecePosition piecePosition;
}
